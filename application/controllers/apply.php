<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Apply extends CI_Controller {
	
	public function job()
	{
		//open for public
		$this->load->library('session');
		$this->load->library('form_validation');
		$this->load->helper('string');		
		$this->load->helper('url');
		$this->load->helper('cookie');
		
		
		//employers cannot apply
		if ($this->session->userdata('user_group') == 'employer') {
			echo 'Employers cannot apply.';
			exit;
		}
		
		$validation_data = array(
			array(
				'field' => 'name',
				'label' => 'Name',
				'rules' => 'trim|required|xss_clean'
			),		
			array(
				'field' => 'email',
				'label' => 'Email Address',
				'rules' => 'trim|xss_clean|strtolower'
			),
			array(
				'field' => 'contact_numbers',
				'label' => 'Contact Number(s)',
				'rules' => 'trim|required|xss_clean'
			),
			array(
				'field' => 'current_address',
				'label' => 'Current Address',
				'rules' => 'trim|xss_clean'
			),
			array(
				'field' => 'cover_letter',
				'label' => 'Cover Letter',
				'rules' => 'xss_clean|callback__check_experience|callback__check_signature'
			),
			array(
				'field' => 'captcha',
				'label' => 'Captcha',
				'rules' => 'xss_clean|trim|callback__check_captcha'
			)				
		);
		$this->form_validation->set_rules($validation_data);
		//validate

		function _submit_errors($error) {
			//require the resume field if the applicant has attached a file
			if ($_FILES['userfile']['size'] != 0) {
				//there was an attached file
				$has_resume = TRUE;
			}
			else {
				//no attached file
				$has_resume = FALSE;
			}
			$arr = array('form_errors' => $error,
							'flash_name' => $_POST['name'],
							'flash_email' => $_POST['email'],
							'flash_contact_numbers' => $_POST['contact_numbers'],
							'flash_current_address' => $_POST['current_address'],
							'flash_cover_letter' => $_POST['cover_letter'],
							'flash_has_resume' => $has_resume);
			return $arr;
		}
		
		if ($this->form_validation->run() == FALSE) {
			//display the form again
			//use flashdata to contain the error
			
			$this->session->set_flashdata(_submit_errors(validation_errors()));
			redirect($_SERVER['HTTP_REFERER'].'?apply=true&rand='.mt_rand());
		}
		else {
			//check if userfile exists
			if ($_FILES['userfile']['size'] != 0) {
				//upload
				//random logo path and filename
				$random_path = strtolower(random_string('alpha', 1));
				$random_filename = random_string('alnum', 16);				
				//upload file
				$config['upload_path'] = './uploads/resumes/'.$random_path;
				$config['allowed_types'] = 'pdf|doc|docx|odt|rtf';
				$config['file_name'] = $random_path.$random_filename;
				$config['max_size']	= '1000';
				$this->load->library('upload', $config);
				if (!$this->upload->do_upload()) {
					//upload error
					$upload_error = TRUE;
					$this->session->set_flashdata(_submit_errors($this->upload->display_errors()));
					redirect($_SERVER['HTTP_REFERER'].'?apply=true&rand='.mt_rand());
				}
				else {
					$upload_error = FALSE;
					//capture the upload data
					$uploaded_file = $this->upload->data();
                    $uploaded_resume_path = $config['upload_path'].'/'.$uploaded_file['file_name'];
                    
                    //upload to Amazon S3
                    $path_parts = pathinfo($_FILES["userfile"]["name"]);
                    $final_file_name = $random_path.$random_filename.'.'.$path_parts['extension'];
                    $reconstructed_file_path_name = 'main/resume/'.$random_path.'/'.$final_file_name;
                    $this->load->library('s3');
                    $temp_file_path = $_FILES['userfile']['tmp_name'];
                    $s3_upload_result = $this->s3->putObject($this->s3->inputFile($temp_file_path), 'companypinboard', $reconstructed_file_path_name);
                    if ($s3_upload_result) {
                        $upload_error = FALSE;
                    }
                    else {
                        $upload_error = TRUE;
                        $this->_deleteUploadedFileOnServer($uploaded_resume_path);
                        $this->session->set_flashdata(_submit_errors('There was a problem uploading the resume. Please try again later.'));
                        log_message('error', 'There was a problem uploading the resume to Amazon S3');
                        redirect($_SERVER['HTTP_REFERER'].'?apply=true&rand='.mt_rand());
                    }
				}
			}
			else {
				$upload_error = FALSE;
				$uploaded_file['file_name'] = '';
			}
			
			//do not continue if there is an upload error
			if($upload_error == FALSE) {
				//get the employerId
				$this->load->model('Apply_Model');
				$employer_id = $this->Apply_Model->getEmployerId($this->uri->segment(3));
                $shortlisted = $this->Apply_Model->getShortlisted($this->uri->segment(3));
                if ($shortlisted == 1) {
                    $status = 0;
                }
                else {
                    $status = 1;
                }
				if ($employer_id) {
					//prepare data to database
					$application_details = array(
						'name' => $this->input->post('name'),
						'email' => $this->input->post('email', TRUE),
						'contactNumbers' => $this->input->post('contact_numbers'),
						'currentAddress' => $this->input->post('current_address'),
						'coverLetter' => $this->input->post('cover_letter'),
						'resume' => $uploaded_file['file_name'],
						'jobSeekerId' => $this->session->userdata('id'),
						'jobID' => $this->uri->segment(3),
						'employerId' => $employer_id,
						'dateSubmitted' => time(),
                        'isJobShortlisted' => $shortlisted,
                        'status' => $status
					);
					//update db
					if ($this->Apply_Model->insertApplication($application_details)) {
						//check if employer wants to receive email
						if ($this->Apply_Model->checkSendJobToEmail($employer_id)) {
                            if ($_FILES['userfile']['size'] != 0) {
                                $uploaded_file_name = $uploaded_file['file_name'];
                                $uploaded_file_ext = $uploaded_file['file_ext'];
                            }
                            else {
                                $uploaded_file_name = FALSE;
                                $uploaded_file_ext = FALSE;
                            }
                            
                            //don't send an email if isShortlisted = 1
                            if (!$shortlisted) {
                                $email_result = $this->_sendEmail($this->uri->segment(3),
                                                                  $this->input->post('job_title', TRUE),
                                                                  $application_details,
                                                                  $uploaded_file_name,
                                                                  $uploaded_file_ext);
                            }
                            else {
                                $email_result = TRUE;
                            }
						}
						else {
							$submit_complete = TRUE;
							$email_result = FALSE;
						}
						
						if ($email_result || $submit_complete) {
							//proceed to next page
							$data = array('upload_success' => 'Your application has been sent. Please visit our Home Page to apply for more jobs. You may now close this window.',
										'page_title' => 'Success',
										'raffle' => $this->input->cookie('raffle'),
										);
							$this->load->view('apply/success', $data);
						}
						else {
							log_message('error', 'A certain job application was not sent to the employer. Debug Details: '.$this->email->print_debugger());
						}
					}
					else {
                        $this->_deleteUploadedFileOnServer($uploaded_resume_path);
						//display error
						$this->session->set_flashdata(_submit_errors('There was an error submitting your application. Please email support@companypinboard.com.'));
						redirect($_SERVER['HTTP_REFERER'].'?apply=true&rand='.mt_rand());
					}
				}
                if (!empty($uploaded_resume_path)) {
                    $this->_deleteUploadedFileOnServer($uploaded_resume_path);
                }
            }
		}
	}

    protected function _deleteUploadedFileOnServer($file_path)
    {
        if (!empty($file_path)) {
            unlink($file_path);
        }
    }
	
	public function _check_captcha($str)
	{
		$this->load->helper('captcha');
		// First, delete old captchas
		$expiration = time()-7200; // Two hour limit
		$this->db->query("DELETE FROM captcha WHERE captcha_time < ".$expiration);	

		// Then see if a captcha exists:
		$sql = "SELECT COUNT(*) AS count FROM captcha WHERE word = ? AND ip_address = ? AND captcha_time > ?";
		$binds = array($str, $this->input->ip_address(), $expiration);
		$query = $this->db->query($sql, $binds);
		$row = $query->row();

		if ($row->count == 0) {
			$this->form_validation->set_message('_check_captcha', 'You did not enter the correct Security Check numbers.');
			return FALSE;		
		}
		else {
			return TRUE;
		}
	}
	
	public function _check_experience($str)
	{
		if (strpos($str,'[Short description of your relevant experience') !== FALSE) {
			$this->form_validation->set_message('_check_experience', 'Oops, you forgot to specify your relevant experiences within your Cover Letter. If you do not have any experiences, please remove the text <b>"[Short description of your relevant experience or achievements and contact information here]"</b> from your Cover Letter.');						
			return FALSE;
		}
		else {
			return TRUE;
		}	
	}
	
	public function _check_signature($str)
	{
		if (strpos($str,'[your name]') !== FALSE) {
			$this->form_validation->set_message('_check_signature', 'Oops, you forgot to indicate your name at the bottom of your cover letter!');						
			return FALSE;
		}
		else {
			return TRUE;
		}
	}

    //duplicate method in shortlisted_app.php
    protected function _sendEmail($job_id, $job_title, $application_details, $uploaded_file_name = FALSE, $uploaded_file_ext = FALSE)
    {
        $this->load->model('Apply_Model');
        //the included file contains $email_body 
        include('./application/helpers/application_email_template_helper.php');
        $this->load->library('email');
        //check if there's attached resume or none
        if ($_FILES['userfile']['size'] != 0) {
            $this->email->attach($config['upload_path'].'/'.$uploaded_file_name, 'attachment', 'resume'.$uploaded_file_ext);
        }
        $this->email->from('support@companypinboard.com', 'Company Pinboard');
        
        //get the job application submit email
        $job_application_email = $this->Apply_Model->getJobAppicationEmail($job_id);
        $this->email->to($job_application_email);
        
        //get the cc emails
        $cc_emails = $this->Apply_Model->getCcEmails($job_id);
        if (is_array($cc_emails)) {
            if (count($cc_emails) > 0) {
                $cc_emails = implode(',', $cc_emails);
                $this->email->cc($cc_emails);
            }
        }
        
        $this->email->reply_to($application_details['email']);
        $this->email->subject('New Job Application for "'.$job_title.'"');
        $this->email->message($email_body);	
        $email_result = $this->email->send();
        if ($email_result) {
            return true;
        }
        else {
            return false;
        }
    }
}

/* End of file apply.php */
/* Location: ./application/controllers/apply.php */